<?php

namespace Backtheweb\GeoIp\Tests\Unit;

use Backtheweb\GeoIp\Models\Location;
use Backtheweb\GeoIp\Services\MaxMindService;
use Backtheweb\GeoIp\Tests\TestCase;
use GeoIp2\Model\City;
use GeoIp2\Model\Country;

class MaxMindTest extends TestCase
{

    /** @test **/
    public function get_ip()
    {
        $user     = env('GEO_IP_MAXMIND_USER_ID');
        $key      = env('GEO_IP_MAXMIND_LICENSE_KEY');
        $database = env('GEO_IP_MAXMIND_DATABASE');
        $ip       = env('GEO_IP_TEST_IP');
        $city     = env('GEO_IP_TEST_CITY');
        $country  = env('GEO_IP_TEST_COUNTRY');
        $service  = new MaxMindService($user, $key, $database);

        $location = $service->location($ip);

        $this->assertTrue(condition: $location instanceof Location);
        $this->assertTrue(condition: $location->country === $country);

        dump($location->toArray());
        ob_flush();
    }

    /** @test_x **/
    public function geo_city()
    {
        $user     = env('GEO_IP_MAXMIND_USER_ID');
        $key      = env('GEO_IP_MAXMIND_LICENSE_KEY');
        $ip       = env('GEO_IP_TEST_IP');
        $city     = env('GEO_IP_TEST_CITY');
        $country  = env('GEO_IP_TEST_COUNTRY');
        $service  = new MaxMindService($user, $key);
        $response = $service->city($ip);

        $this->assertTrue(condition: $response instanceof City);
        $this->assertTrue(condition: $response->city->name    === $city);
        $this->assertTrue(condition: $response->country->name === $country);
    }

    /** @test_x **/
    public function geo_country()
    {
        $user     = env('GEO_IP_MAXMIND_USER_ID');
        $key      = env('GEO_IP_MAXMIND_LICENSE_KEY');
        $ip       = env('GEO_IP_TEST_IP');
        $country  = env('GEO_IP_TEST_COUNTRY');
        $service  = new MaxMindService($user, $key);
        $response = $service->city($ip);

        $this->assertTrue(condition: $response instanceof Country);
        $this->assertTrue(condition: $response->country->name === $country);
    }
}
