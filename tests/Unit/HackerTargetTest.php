<?php

namespace Backtheweb\GeoIp\Tests\Unit;

use Backtheweb\GeoIp\Models\Location;
use Backtheweb\GeoIp\Services\HackerTargetService;
use Backtheweb\GeoIp\Tests\TestCase;

class HackerTargetTest extends TestCase
{

    /** @test **/
    public function get_ip()
    {
        $ip       = env('GEO_IP_TEST_IP');
        $service  = new HackerTargetService();
        $location = $service->location($ip);

        $this->assertTrue($location instanceof Location);

        dump($location->toArray());
        ob_flush();
    }
}
