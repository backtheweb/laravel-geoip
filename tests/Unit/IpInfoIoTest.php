<?php

namespace Backtheweb\GeoIp\Tests\Unit;

use Backtheweb\GeoIp\Models\Location;
use Backtheweb\GeoIp\Services\IpInfoIoService;
use Backtheweb\GeoIp\Tests\TestCase;

class IpInfoIoTest extends TestCase
{

    /** @test **/
    public function get_ip()
    {
        $ip       = env('GEO_IP_TEST_IP');
        $token    = env('GEO_IP_IPINFOIO_TOKEN');
        $service  = new IpInfoIoService($token);
        $location = $service->location($ip);

        $this->assertTrue(condition: $location instanceof Location);

        dump($location->toArray());
        ob_flush();
    }
}
