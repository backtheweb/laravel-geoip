Laravel GeoIp 
-------------

## Available services

* [MaxMind](http://maxmind.com/) from $0.0001 per query
* [Hacker Target](https://hackertarget.com/geoip-ip-location-lookup/) Free with limited requests per day
* [Ipinfo.io](https://ipinfo.io/) Free plan is limited to 50k requests per month

## Installation

```bash
composer require backtheweb/laravel-geoip
 
php artisan vendor:publish --provider="Backtheweb\GeoIp\GeoIpServiceProvider" --tag="config"
```

Edit `geoip.php` in config folder or ser environment vars.

```php
return [

    'default' => env('GEO_IP_SERVICE', 'maxmind'),
    'services' => [
        'maxmind' => [
            'user' => env('GEO_IP_MAXMIND_USER_ID'),
            'key'  => env('GEO_IP_MAXMIND_LICENSE_KEY'),
            'database' => env('GEO_IP_MAXMIND_DATABASE', 'country'),
        ],
        'hackertarget' => [],
        'ipinfoio'     => [
            'token' => env('GEO_IP_IPINFOIO_TOKEN'),
        ],
    ],

    'test' => [
        'ip'      => env('GEO_IP_TEST_IP'),
        'city'    => env('GEO_IP_TEST_CITY'),
        'country' => env('GEO_IP_TEST_COUNTRY'),
    ]
];
```

## Usage

```php
use Backtheweb\GeoIp\Facades\GeoIp
$location = GeoIp::location('172.105.90.46');
$location->toArray(); 
```

Sample output of `$location->toArray()`

```php 
array [ 
  "ip" => "172.105.90.46"
  "hostname" => null
  "country" => "Germany"
  "country_code" => "DE"
  "state" => "Hesse"
  "city" => "Frankfurt am Main"
  "postal_code" => "60313"
  "lat" => 50.1188
  "lng" => 8.6843
  "timezone" => "Europe/Berlin"
  "provider" => "Linode"
]
```

### Available response properties

| 	             | MaxMind\**	 | Hacker Target | IpInfo.io |
|---------------|:-----------:|:-------------:|:---------:|
| ip	           |      ✓      |       ✓       |     ✓     |
| hostname	     |      ✓      |       	       |     	     |
| country	      |      ✓      |       ✓       |     ✓     |
| country_code	 |      ✓      |       ✓       |     ✓     |
| postal_code 	 |      ✓      |       ✓       |     ✓     |
| lat	          |      ✓      |       ✓       |     ✓     |
| lng	          |      ✓      |       ✓       |     ✓     |
| timezone	     |      ✓      |       	       |     ✓     |
| provider	     |      ✓      |       	       |     ✓     |

** Unsing insights database

## Testing

create a `.env` file with the following content

```bash
GEO_IP_TEST_IP=172.105.90.46
GEO_IP_TEST_CITY="Hesse"
GEO_IP_TEST_COUNTRY=Germany 
```
and run

```bash
composer test
```
