<?php

namespace Backtheweb\GeoIp\Models;

class Location
{

    public ?string $ip            = null;
    public ?string $hostname      = null;
    public ?string $country       = null;
    public ?string $country_code  = null;
    public ?string $state         = null;
    public ?string $city          = null;
    public ?string $postal_code   = null;
    public ?string $timezone      = null;
    public ?string $provider      = null;
    public float $lat             = 0;
    public float $lng             = 0;

    public static function hydrate(array $geo = []) : Location
    {
        $l = new Location;

        foreach ($geo as $attr => $value) {

            if(property_exists(self::class, $attr)){

                $l->$attr = $value;
            }
        }

        return $l;
    }

    public function toArray(): array
    {
        return [
            'ip'           => $this->ip,
            'hostname'     => $this->hostname,
            'country'      => $this->country,
            'country_code' => $this->country_code,
            'state'        => $this->state,
            'city'         => $this->city,
            'postal_code'  => $this->postal_code,
            'lat'          => $this->lat,
            'lng'          => $this->lng,
            'timezone'     => $this->timezone,
            'provider'     => $this->provider,
        ];
    }
}
