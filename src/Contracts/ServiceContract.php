<?php

namespace Backtheweb\GeoIp\Contracts;

use Backtheweb\GeoIp\Models\Location;

interface ServiceContract
{
    public function location(string $ip) : Location;
}
