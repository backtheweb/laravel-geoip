<?php

namespace Backtheweb\GeoIp;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;

class GeoIpServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../config/geoIp.php' => config_path('geoIp.php')],   'config');

        //if ($this->app->runningInConsole()) {
        //    $this->commands([]);
        //}
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/geoIp.php', 'geoIp');

        $this->app->bind('backtheweb.geoIp', function(Application $app) {

            return match($app->get('geoIp.default')) {
                'maxmind'      => new Services\MaxMindService(
                    $app->get('geoIp.services.maxmind.user'),
                    $app->get('geoIp.services.maxmind.key'),
                    $app->get('geoIp.services.maxmind.database', 'country'),
                    $app->get('geoIp.services.maxmind.locales', ['en']),
                    $app->get('geoIp.services.maxmind.options', []),
                ),
                'ipinfoio'     => new Services\IpInfoIoService(
                    $app->get('geoIp.services.ipinfoio.token'),
                ),
                'hackertarget' => new Services\HackerTargetService(),
                default        => throw new \Exception('Service not found'),
            };
        });
    }

    public function provides()
    {
        return ['backtheweb.geoIp'];
    }
}
