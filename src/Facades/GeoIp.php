<?php

namespace Backtheweb\GeoIp\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \GeoIp2\Model\City city(string $ip)
 * @method static \GeoIp2\Model\Country country(string $ip)
 *
 */
class GeoIp extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.geoIp';
    }
}
