<?php

namespace Backtheweb\GeoIp\Services;

use Backtheweb\GeoIp\Contracts\ServiceContract;
use Backtheweb\GeoIp\Models\Location;
use GeoIp2\Model\Insights;
use GeoIp2\WebService\Client;
use GeoIp2\Model\City;
use GeoIp2\Model\Country;

class MaxMindService implements ServiceContract
{
    protected Client $client;

    protected string $database;

    /**
     * @param string $user
     * @param string $key
     * @param string $database
     * @param array $locales
     * @param array $options
     *
     * @throws \Exception
     */
    public function __construct(
        #[\SensitiveParameter] string $user,
        #[\SensitiveParameter] string $key,
        string $database = 'country',
        array $locales = ['en'],
        array $options = []

    )
    {
        $this->client = new Client(accountId: $user, licenseKey: $key, locales: $locales, options: $options);

        if(!in_array($database, ['city', 'country', 'insights'])){
            throw new \Exception('Invalid database');
        }

        $this->database = $database;

        dd($this->database);
    }

    /**
     * @param string $ip
     * @return City
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \GeoIp2\Exception\AuthenticationException
     * @throws \GeoIp2\Exception\GeoIp2Exception
     * @throws \GeoIp2\Exception\HttpException
     * @throws \GeoIp2\Exception\InvalidRequestException
     * @throws \GeoIp2\Exception\OutOfQueriesException
     */
    public function city(string $ip) : City
    {
        return $this->client->city(ipAddress: $ip);
    }

    /**
     * @param string $ip
     * @return Country
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \GeoIp2\Exception\AuthenticationException
     * @throws \GeoIp2\Exception\GeoIp2Exception
     * @throws \GeoIp2\Exception\HttpException
     * @throws \GeoIp2\Exception\InvalidRequestException
     * @throws \GeoIp2\Exception\OutOfQueriesException
     */
    public function country(string $ip) : Country
    {
        return $this->client->country(ipAddress: $ip);
    }

    public function insights(string $ip) : Insights
    {
        return $this->client->insights(ipAddress: $ip);
    }

    /**
     * @param string $ip
     * @return array
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \GeoIp2\Exception\AuthenticationException
     * @throws \GeoIp2\Exception\GeoIp2Exception
     * @throws \GeoIp2\Exception\HttpException
     * @throws \GeoIp2\Exception\InvalidRequestException
     * @throws \GeoIp2\Exception\OutOfQueriesException
     */
    public function location(string $ip): Location
    {
        $record = $this->{ $this->database }($ip);
        $geo    = $this->parse($record);

        return Location::hydrate($geo);
    }

    /**
     * @throws \Exception
     */
    public function raw(string $ip): array
    {
        throw new \Exception('Not implemented');
    }

    /**
     * @param Insights $record
     * @return array
     */
    protected function parse(Insights|City|Country $record) : array
    {
        return [
            'ip'           => $record->traits->ipAddress,
            'country'      => $record->country->name,
            'country_code' => $record->country->isoCode,
            'state'        => $record instanceof City ? $record->subdivisions[0]->name  : null,
            'city'         => $record instanceof City ? $record->city->name             : null,
            'lat'          => $record instanceof City ? $record->location->latitude     : 0,
            'lng'          => $record instanceof City ? $record->location->longitude    : 0,
            'timezone'     => $record instanceof City ? $record->location->timeZone     : null,
            'provider'     => $record->traits->isp,
            'postal_code'  => $record instanceof City ? $record->postal->code           : null,
        ];
    }
}
