<?php

namespace Backtheweb\GeoIp\Services;

use Backtheweb\GeoIp\Contracts\ServiceContract;
use Backtheweb\GeoIp\Models\Location;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Storage;

class IpInfoIoService implements ServiceContract
{

    protected Client $client;

    public function __construct(
        #[\SensitiveParameter] string $token = null
    )
    {
        $auth   = ['Authorization'  => $token ? 'Bearer ' . $token . ' ipinfo.io' :  null];
        $accept = ['Accept'         => 'application/json ipinfo.io'];

        $this->client = new Client([
            'base_uri' => 'https://ipinfo.io/',
            'headers'  => [

                ...$auth,
                ...$accept
            ]
        ]);
    }

    /**
     * @param string $ip
     * @return string
     * @throws GuzzleException
     */
    public function location(string $ip) : Location
    {
        $response = $this->client->get($ip);

        $geo      = $this->parse($response->getBody()->getContents() ?? '{}');

        return Location::hydrate($geo);
    }

    /**
     * @param string $ip
     * @return array
     * @throws GuzzleException
     */
    public function get(string $ip) : array
    {
        $response = $this->client->get($ip);

        return json_decode($response->getBody()->getContents(),true);
    }

    /**
     * Convert a json string response to an array
     *
     * @param string $json
     * @return array
     */
    private function parse(string $json) : array
    {
        $geo  = collect();
        $keys = [
            'ip'         => 'ip',
            'country'    => 'country_code',
            'city'       => 'city',
            'region'     => 'state',
            'postal'     => 'postal_code',
            'org'        => 'provider',
            'loc'        => ['lat', 'lng'],
            'timezone'   => 'timezone',
            'hostname'   => 'hostname',
        ];

        collect(json_decode($json, true))->each(function ($value, $key) use($keys, $geo) {

            switch ($key){
                case 'loc':

                    list($lat, $lng) = explode(',', $value);

                    $geo->put('lat', (float) $lat);
                    $geo->put('lng', (float) $lng);

                    break;

                case 'country':

                    $geo->put('country_code', $value);
                    $country = null;

                    try {

                        $geo->put('country', collect(require(__DIR__ . '/../data/countries.php') ?? [])->first(function ($item, $key) use ($geo) {
                            return $key == $geo['country_code'];
                        }));

                    } catch (\Exception $e) {
                        $geo->put('country', null);
                    }

                    break;

                default:

                    if(array_key_exists($key, $keys)){
                        $geo->put($keys[$key], $value);
                    }
            }
        });

        return $geo->toArray();
    }
}
