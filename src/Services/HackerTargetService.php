<?php

namespace Backtheweb\GeoIp\Services;

use Backtheweb\GeoIp\Contracts\ServiceContract;
use Backtheweb\GeoIp\Models\Location;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class HackerTargetService implements ServiceContract
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.hackertarget.com/'
        ]);
    }

    /**
     * @param string $ip
     * @return string
     * @throws GuzzleException
     */
    public function location(string $ip) : Location
    {
        $response = $this->client->get('ipgeo', [
            'query' => [
                'q' => $ip
            ]
        ]);

        $content = $response->getBody()->getContents();
        $geo     = $this->parse($content);

        return Location::hydrate($geo);
    }

    public function get(string $ip) : array
    {
        $response = $this->client->get('ipgeo', [
            'query' => [
                'q' => $ip
            ]
        ]);

        $reader = function($data) {
            foreach (explode(PHP_EOL, $data) as $line) {
                list($key, $value) = explode(':', $line);
                yield [$key => trim($value)];
            }
        };

        return $reader($response->getBody()->getContents());
    }

    /**
     * Convert a text response to an array
     * Input text example:
     *
     *  IP Address: 185.30.162.242
     *  Country: Spain
     *  State: Girona
     *  City: Lloret de Mar
     *  Latitude: 41.6967
     *  Longitude: 2.8486
     *
     * @param string $data
     * @return array
     */
    private function parse(mixed $data) : array
    {
        /**
         * Convert text to array
         * @param string $data
         * @return \Generator
         */
        $reader = function($data) {
            $keys = [
                'IP Address' => 'ip',
                'Country'    => 'country',
                'State'      => 'state',
                'City'       => 'city',
                'Latitude'   => 'lat',
                'Longitude'  => 'lng',
            ];

            foreach (explode(PHP_EOL, $data) as $line) {
                list($key, $value) = explode(':', $line);

                if(array_key_exists($key, $keys)){
                    yield [$keys[$key] => trim($value)];
                }
            }
        };

        // Parse array

        $geo = collect($reader($data))->mapWithKeys(function ($item) {

            if (isset($item['lat'])) {
                $item['lat'] = (float) $item['lat'];
            }

            if (isset($item['lng'])) {
                $item['lng'] = (float) $item['lng'];
            }

            return $item;
        });

        // Add country code from country name

        try {
            $code = fn() => array_search($geo['country'], require(__DIR__ . '/../data/countries.php'));
            $geo->put('country_code', $code());
        } catch (\Exception $e) {
            $geo->put('country_code', null);
        }

        return $geo->toArray();
    }
}
