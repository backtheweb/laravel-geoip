<?php

return [

    'default' => env('GEO_IP_SERVICE', 'maxmind'),
    'services' => [
        'maxmind' => [
            'user'     => env('GEO_IP_MAXMIND_USER_ID'),
            'key'      => env('GEO_IP_MAXMIND_LICENSE_KEY'),
            'database' => env('GEO_IP_MAXMIND_DATABASE', 'insights'), // country, city, insights
        ],
        'hackertarget' => [],
        'ipinfoio'     => [
            'token' => env('GEO_IP_IPINFOIO_TOKEN'),
        ],
    ],

    'test' => [
        'ip'      => env('GEO_IP_TEST_IP'),
        'city'    => env('GEO_IP_TEST_CITY'),
        'country' => env('GEO_IP_TEST_COUNTRY'),
    ]
];
